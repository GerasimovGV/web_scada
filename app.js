var g;
var o;
var s;
var cnt = 0;
var SWDataSource = null;//Источник данных
var SwDataClient = null;//Потребитель данных
var svgContents = null;//массив SVG изображения с доступом по ключу

window.onload = function() {
    console.log('Документ и все ресурсы загружены');
    svgContents = new TSvgContents();//создаю массив SVG изображений с доступом по ключу
    svgContents.getImg('switchOn'   , './img/switchOn.svg');
    svgContents.getImg('switchOff'  , './img/switchOff.svg');
    svgContents.getImg('switchNoLink', './img/switchNoLink.svg');
    console.warn(svgContents.aContents);
    //создаю объект Источник данных
    SWDataSource = new TSwitchDataSource();
    //создаю объект Потребитель данных
    SwDataClient = new TSwitchClient();
    SwDataClient.draw();//
    SwDataClient.subcribe('ON');//
    //задаю ключи svg- рисунков
    SwDataClient.svgArray = svgContents;//передаю массив из которого буду брать изображения
    SwDataClient.stageImgKey.On = "switchOn";
    SwDataClient.stageImgKey.Off = "switchOff";
    SwDataClient.stageImgKey.NoLink = "switchNoLink"
    //прикреплю его к источнику данных
    SWDataSource.addSubscriber(SwDataClient, SwDataClient.subcribe);//
    ////
    g = new TSVGGroups ('svg_motor_remake','CnvScr');
    //получаю SVG-контейнер
    SwDataClient.SVGconteiner = g.getElementByAttrValue('data-id','MainSwitch');//поиск группы svg-контейнера
    //В найденный контейнер надо добавить foreignObject 
    var content = SwDataClient.getImage(SwDataClient.stageImgKey.On);
    //3) добавляю созданый объект в контенйнер на общей картине SVG
    setForeignObject(SwDataClient.SVGconteiner, content);
 };

//потом внесу её в объект TSwitchClient
function SwDataClientDraw(){
  console.warn('SwDataClientDraw',SwDataClient.stage);
  var container = SwDataClient.SVGconteiner;
  if (container == null) return;
  //1) подготовка нового SVG согласно состояния
  var content;
  switch (SwDataClient.stage) {
        case 'ON':
                content = SwDataClient.getImage(SwDataClient.stageImgKey.On);
            break;
        case 'OFF':
                content = SwDataClient.getImage(SwDataClient.stageImgKey.Off);
            break;
        case 'NOLINK':
                content = SwDataClient.getImage(SwDataClient.stageImgKey.NoLink);
            break;            
  }
  //var _svg = content.querySelector('svg');
  //2) разбираюсь с FO
  var fo = container.querySelector('foreignObject');
  if (fo == undefined) {
      //если FO ещё не встроен, то создать и встроить
      //узнаю размеры контейнера
      var box = container.querySelector('rect').getBBox();
      //Создаю FO
      fo = document.createElementNS('http://www.w3.org/2000/svg','foreignObject');
      //Устанавливаю аттрибуты размеров FO
      fo.setAttribute('x', box.x);
      fo.setAttribute('y', box.y);
      fo.setAttribute('width', box.width);
      fo.setAttribute('height', box.height);
      //добавляю FO к контеннеру
      container.appendChild(fo);
  }
  //3) если в FO уже вставлен SVG то надо его удалить и заменить новым
  var svg = fo.querySelector('svg');
    if (svg != undefined) {
        fo.removeChild(svg);
    }
    //теперь добавлю новый svg-элемент в FO
    //svg = content.querySelector('svg');
    fo.appendChild(content);
}

function setForeignObject(container, content){
    console.log(container);
    if (content == undefined) return;
    //Вставка FO получилась, не могу понять почему не получалась
    var fo = document.createElementNS('http://www.w3.org/2000/svg','foreignObject'); 
    var box = container.querySelector('rect').getBBox();
    //надо взять размеры контейнера
    fo.setAttribute('x', box.x);
    fo.setAttribute('y', box.y);
    fo.setAttribute('width', box.width);
    fo.setAttribute('height', box.height);
    fo.appendChild(content);;
    console.log(fo);
    container.appendChild(fo);
    console.log(container);
    //x="0" y="102.047" width="17" height="17"
}

 function check(){
		
    var result = '';
    var radios = document.getElementsByName('stage');
    for (var i = 0, radio; radio = radios[i]; i++) {
        if (radio.checked) result =  radio.value;
    }
    console.log(result);
    SWDataSource.setStage(result);
}

function Clear(){
    document.getElementById('output').innerHTML = '';
}

//drawSvgViewObjects создаёт Аватаров SVG элементов имеющих графическое отображение
//и отображает на заданном CANVAS
function drawSvgViewObjects() {
    o = new TSVGAvatars ('svg_motor_remake');//создать Аватаров заданного SVG
    f = new TLoadCfs("./json/mainswitch.json");
    f.loadJSONFile();
    //o.draw('CnvScr');//показать все Аватары
    //o.drawBound('CnvScr');//показать все Аватары
    //console.warn(o);//показать объекты
    var svg = document.getElementById('svg_motor_remake');
    var scd = svg.contentDocument;
    scd.addEventListener("mousedown", onClickDIV); //добить обрабоботчик click
}

function onClickDIV(e){
    var x = e.offsetX==undefined?e.layerX:e.offsetX;
    var y = e.offsetY==undefined?e.layerY:e.offsetY;
    var p = g.inBound(x,y,3);//получить объект
    var a = o.inBound(x,y,3);//получить объект
    if (a != undefined) {
        console.warn(a);
        a.classList.value = 'st4';
    }
    console.log('Click',' X:',x,' Y:',y);
    o.inArea;
}


		/*
		var a = searchSvgViewObjects('svg_motor_remake');//создал список отображаемых элементов SVG
		var aAvatars = createSvgAvatarObjects (a);//из них сделал список (массив) Аватаров
		//console.warn(aAvatars);
		showSvgAvatars(aAvatars);//показать полученные объекты
		*/
		//теперь надо создать именно аватаров, обращаясь к которым методом Draw я сразу отображаю их на требуемый CANVAS
			//доступ к аттрибутам
			/*
			в SVG:
			<circle r="50" cx="50" cy="50" fill="red"/>
			в JS
			var circle = document.querySelector(«circle»); //тут способ доступа может отличаться
			circle.setAttribute(«fill», «cyan»);
			*/
			
			//пример вывода:
			//<g id="shape80-255" transform="translate(62.3622,-90.7087)">
			//	<title>Sheet.80</title>
			//	<desc>WARNING</desc>
			//	<rect x="0" y="260.787" width="56.6929" height="11.3386" class="st1"/>
			//	<text x="15.46" y="268.26" class="st16">WARNING</text>
			//</g>
			  //console.log(svg_element.children);
			//пример вывода
			//(4) [title, desc, rect, text]
			//	>0:title
			//	>1:desc
			//	>2:rect
			//	>3:text
			//	length:4
			//	>__proto__:HTMLCollection
			  //console.log(svg_element.attributes);
			//Пример вывода:
			//NamedNodeMap {0: id, 1: transform, length: 2}
			//	>0:id
			//	>1:transform
			//	length:2
			//	>__proto__:NamedNodeMap

            function addUserEvent(event, id) {//получение доступа к элементу SVG и доавление к нему обработки события click
                //var s = document.getElementById('svg_led');
                //var sd = s.contentDocument;//получить контент SVG рисунка
                //var se = sd.getElementById(id);//найти элемент с заданным ID
                //se.addEventListener("mousedown", LogOut); //добить обрабоботчик mousedown
                //var se = document.getElementById('ball');
                //se.onclick = SetLedOff;
                //se.addEventListener("click", SetLedOff); //добить обрабоботчик click
        
                //var s = document.getElementById('svg_motor_remake');
                //s.addEventListener("mousedown", onClickDIV); //добить обрабоботчик click
                
                //s.addEventListener("mouseover", GetInfo); //добить обрабоботчик click
                /*
                s.contentDocument.addEventListener("click", svgmouseclick); //добить обрабоботчик click
                s.contentDocument.addEventListener("mousedown", svgmousedown); //добить обрабоботчик click
                s.contentDocument.addEventListener("mouseup", svgmouseup); //добить обрабоботчик click
                s.contentDocument.addEventListener("mousemove", svgmousemove); //добить обрабоботчик click
                */
            };
            
            var svgclick = true;
            
            /*
            var elementsAt = function( x, y ){
                var s = document.getElementById('svg_motor_remake');
                var elements = [], current = s.elementFromPoint( x, y );
                while( current &&  current.nearestViewportElement ){
                    elements.push( current );
                    current.style.display = "none";
                    current = s.elementFromPoint( x, y );
                }
                elements.forEach( function( elm ){
                    elm.style.display = ''; 
                });
                return elements;
            }
         */
            
            function svgmouseclick(e){
              var s = document.getElementById('svg_motor_remake');
                console.log(s.contentDocument.children);//выводит всё содержимое SVG объекта 
                //в консоли это выглядит так:
                //[svg] это дерево которое я могу разворачивать
                //а так как в SVG ничего кроме svg нет, то элемент svg будет иметь индекс [0]
                //И в дальнейшем я буду обращаться по этому индексу
                console.log(s.contentDocument.children[0]);//
                //вот так это выглядит в консоли
                //	<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="42mm" height="96mm" viewBox="0 0 119.055 272.126">
                //		<style type="text/css"> ... </style>
                //		<g>...</g>
                //	</svg>
                //меня интересуют элементы в svg, пытаюсь до них добраться
                console.log(s.contentDocument.children[0].children);//выводит элементы svg, например у меня есть элементы style и g		
                //в консоли будет [style, g] их можно открывать разглядывая внутреннее устройство, если раскрыть то будет
                //	>0: style
                //	>1: g
                //	lenght: 2
            //как я понимаю, далее нужно по индексу с ними общаться, найти индеск для style он не обязательно может быть 0
                //и индексы для всего остального, так как кроме g могут быть и другие элементы SVG (сейчс они в недрах тегов g)
                //отличить их можно по tagName = g;
                //Интересно, работает ли поиск по tagName) проверим:
                //getElementsByTagNameNS - не работает
                console.log(s.contentDocument.children[0].getElementsByTagName('*'));//выводит вообще все элементы содержащиеся в документе 297шт!
                console.log(s.contentDocument.children[0].getElementsByTagName('style'));//только элементы из то что в style 1шт
                var g_elements = s.contentDocument.children[0].getElementsByTagName('g');
                console.log(g_elements);//только элементы c тегом g 83шт, почему? визуально их больше
                //теперь выведу в консоль все полученные элементы с тегом g
                console.log(g_elements.length);
                var i = g_elements.length;
                var svg_element = {};
                while (i != 0) {
                    i--;
                    svg_element = g_elements[i];
                    console.log(svg_element);
                    //пример вывода:
                    //<g id="shape80-255" transform="translate(62.3622,-90.7087)">
                    //	<title>Sheet.80</title>
                    //	<desc>WARNING</desc>
                    //	<rect x="0" y="260.787" width="56.6929" height="11.3386" class="st1"/>
                    //	<text x="15.46" y="268.26" class="st16">WARNING</text>
                    //</g>
                    console.log(svg_element.children);
                    //пример вывода
                    //(4) [title, desc, rect, text]
                    //	>0:title
                    //	>1:desc
                    //	>2:rect
                    //	>3:text
                    //	length:4
                    //	>__proto__:HTMLCollection
                    console.log(svg_element.attributes);
                    //Пример вывода:
                    //NamedNodeMap {0: id, 1: transform, length: 2}
                    //	>0:id
                    //	>1:transform
                    //	length:2
                    //	>__proto__:NamedNodeMap
                }
                //у всех тегов g есть свойство transform(x,y) через которую можно перемещать g в пределах svg
                //	transform="translate(31.0394,-62.3622)">
        
                //console.log(s.contentDocument.children[0]);
              //var children = s.contentDocument.children[0].children ; //getElementsByTagName('*');
              //var o = e.target.elementFromPoint(e.clientX,e.clientY);
              //var o = elementsAt(e.clientX, e.clientY); 
              svgclick = false;
              //console.log('click:'+'x='+e.clientX+' y='+e.clientY+' o:'+o);
                //console.log(children);
            }
            
            function svgmousedown(e){
              svgclick = true;
              console.log('down');
            }
            
            function svgmouseup(e){
              if (svgclick)
                svgmouseclick(e);
              else
                console.log('up');
            }	
            
            function svgmousemove(e){
              svgclick = false;
              console.log('move');
            }	
            
            function GetElementInfo(e) {
              console.log(e.target);
              return false;  
            }
            
            function GetObjectInfo(o) {
              var s = o.getElementsByTagName('*');
              console.log(s);
              document.getElementById('output').innerHTML = s;
            }
            
            function GetInfo(e){
              document.getElementById('output').innerHTML = e.target +': '+ e.target.tagName;
              GetObjectInfo(e.target);
              //теперь вывести всю информацию о таргет
              return false;
            }
            
            function SetLedOff() {//обработчик click на отключние светодиода
                this.classList.value = 'LedOff';//меняю стиль на "отключено" и светодиод становится серым
                //this.removeEventListener("click", SetLedOff);//удаляю обработчик на отключение
                //this.addEventListener("click", SetLedOn); //добавляю обработчик на включение
                                                        //инача они оба будут в списке обработки
                this.onclick = SetLedOn;
            }
            
            function SetLedOn() {//обработчик click на вкключние светодиода
                this.classList.value = 'LedOn';//меняю стиль на "включено" и светодиод становится красным
                //this.removeEventListener("click", SetLedOn);
                //this.addEventListener("click", SetLedOff); //
                this.onclick = SetLedOff;
            }
        
            function LogOut(event,e) {
                cnt++;
                s += (cnt +': '+event+'-'+e.target +': '+ e.target.tagName+': '+e.target.getAttribute('id') + '<br>');
                document.getElementById('output').innerHTML = s;
            }
            
            var ball = document.getElementById('ball');
        
            var clik = true;
            ball.onmousedown = function(e) {
                LogOut('onmousedown',e);
                ball.style.position = 'absolute';
                document.body.appendChild(ball);
                moveAt(e);
                ball.style.zIndex = 1000; // над другими элементами
                clik = true;
                
                    function moveAt(e) {
                        ball.style.left = e.pageX - ball.offsetWidth / 2 + 'px';
                        ball.style.top = e.pageY - ball.offsetHeight / 2 + 'px';
                    }
                
                document.onmousemove = function(e) {
                    clik = false;
                    moveAt(e);
                };
                
                ball.onmouseup = function(e) {
                    LogOut('onmouseup',e);
                    document.onmousemove = null;
                    ball.onmouseup = null;
                    if (clik) {
                        ball.onclick(e);
                        clik = false;
                    }
                };
            }
            ball.ondragstart = function(e) {
                LogOut('ondragstart',e);
                return false;
            };
            
            ball.ondragstop = function(e) {
                LogOut('ondragstop',e);
                return false;
            };
        
/*  
//асинхронное чтение файла    
function getForeignObject(container) {
    var svgFile = SwDataClient.stageImg.On;
    var req = new XMLHttpRequest();
        req.open("GET", svgFile, true);//true - делаю aсихронный запрос к http-серверу 
        req.overrideMimeType('image/svg+xml; charset=utf-8');
        req.onreadystatechange = callback;
        req.send();

        //вложенная функция, вызывается по мере загрузки файла
        //если успешно файл загружен, то вызывает
        function callback () {
            console.log(req.statusText);
            if (req.readyState != 4) return;
            if (req.status !=200) {//ошибка
                console.log(req.status+':'+req.statusText);
            }
            else {//получил SVG в виде строки
                var s = req.responseText; 
                console.log(s);
                //теперь делаю их него document - объект, чтобы можно было парсить
                var parser = new DOMParser();
                var stringContainingXMLSource = s;
                var doc = parser.parseFromString(stringContainingXMLSource, 'image/svg+xml');
                console.log(doc);
                setForeignObject(container, doc);
                //да, документ получил. Можно парсить
            }
        }
 } */        

  /*
 //12.03.2018 эксперимент по отображению SVG на CANVASе.
 //вот интересный ресурс:
 // https://tristandunn.com/2014/01/24/rendering-svg-on-canvas/
 //<div class="svg_div_motor_block" onclick="onClickDIV(event);">
 //<canvas height='200' width='200' id='svgAvatar'>Обновите браузер</canvas>	
//</div>
 //изображение выводится, но оно имеет размытости по сравнению с SVG вставленным в OBJECT
 //Так-то идея хорошая, сделать фон из большого SVG, найти там контейнеры
 //и в контецнеры выводить мелкие SVG
 //.. может быть ещё вернусь к этой идее, пока но пока пытаюсь вставить SVG в SVG для обеспечения качества графики
 function fillSvgAvatar () {
    //var svg = document.getElementById('svg_motor_remake');//получаю доступ к DOM SVG
    var scr = document.getElementById('svgAvatar');
    var	ctx = scr.getContext('2d');
    //var ratio   = window.devicePixelRatio || 1;
    var img = new Image();  // Создание нового объекта изображения
    img.src = 'img/motor.svg';  // Путь к изображению которое необходимо нанести на холст
        // 1. Ensure the element size stays the same.
        //scr.style.width  = svg.width;// + "px";
        //scr.style.height = svg.height;// + "px";
        // 2. Increase the canvas dimensions by the pixel ratio.
       // ctx.width  *= ratio;
       // ctx.height *= ratio;
    img.onload = function() {
        //ctx.scale(ratio, ratio);
        ctx.drawImage(img,0,0,119.055, 119.055);
    }
 }
*/

//Вставка в SVG
    /*'это сделано по совету уважаемого сэра
    //Способ взят из ответа уважаемого ccprog на мой первый вопрос на stackoverflow.com
    //https://stackoverflow.com/questions/49645184/inserting-svg-files-as-foreignobject-in-to-svg-embedded-to-dom-as-%D0%BEbject/49660496#49660496
    //он советует обойтись без FO так как SVG можо вставить в SVG напрямую (не знаю пока быстрее это или медленне при отображении)
    var box = container.querySelector('rect').getBBox();
    var _svg = content.querySelector('svg');
    _svg.setAttribute('x', box.x);
    _svg.setAttribute('y', box.y);
    _svg.setAttribute('width', box.width);
    _svg.setAttribute('height', box.height);
    _svg.setAttribute('data-id-state','ON');//название картинки
    container.appendChild(_svg);
    console.log(container);
    return;*/
    /*
    
    //Так как не получается отобразить FO, попробую вставить rect
    //вставка RECT прошла успешно rect = document.createElementNS
    var rect = document.createElementNS('http://www.w3.org/2000/svg','rect'); 
    var box = container.querySelector('rect').getBBox();
    rect.setAttribute('x', box.x+2);
    rect.setAttribute('y', box.y+2);
    rect.setAttribute('width', box.width-4);
    rect.setAttribute('height', box.height-4);
    rect.setAttribute('class','st2');//название картинки
    container.appendChild(rect);
    console.log(container);
    return; */

/*
 //эксперимент с таблицей стилей
 function exampleCSS () {
    var svg = document.getElementById('svg_motor_remake');//получаю доступ к DOM SVG
    var stSheet = svg.contentDocument.styleSheets[0];
    console.log(stSheet);
     var crs = stSheet.cssRules;
    console.log(crs);
    //и так у меня есть картинка в SVG, пока это просто файл
}
*/